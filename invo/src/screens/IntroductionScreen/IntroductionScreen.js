import React from 'react';
import {
    View, Button, Text, StyleSheet, Image, KeyboardAvoidingView, TouchableOpacity, TextInput,
} from 'react-native';

//importing images
const headerImage = require('./images/numbers-white-background_hirz2gth_thumbnail-full01.png');
const numbersLogo = require('./images/number1.png');

export default class IntroductionScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headers}>
                    <Image

                        resizeMode='cover'
                        style={styles.imageStyle}
                        source={headerImage}
                    />
                </View>
                <KeyboardAvoidingView >
                    <View style={{ flex: 1}}>
                        <View style={{justifyContent: 'center', alignItems: 'center', paddingTop: 100}}>
                            <Image
                                style={{ height: 50, width: 200  }}
                                resizeMode='stretch'
                                source={numbersLogo}
                            />
                        </View>

                        <View>
                            <TextInput placeholder='E-mail'/>
                            <TextInput placeholder='E-mail'/>
                        </View>

                        <View   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Button title='Hello World'/>
                        </View>
                    </View>
                </KeyboardAvoidingView>

                <View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headers: {
        height: 50,
    },
    imageStyle: {
        flex: 1,
        justifyContent: 'center'
    },
    body: {
        flex: 1,
    }
});