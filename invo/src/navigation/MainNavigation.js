import React from 'react';
import {
    View, Button, Text, StyleSheet
} from 'react-native';
import {
    createSwitchNavigator, createAppContainer
} from 'react-navigation';

// importing screens
import IntroductionScreen from "../screens/IntroductionScreen/IntroductionScreen";

export default class MainNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <AppNavigationContainer />
        );
    }
}

const AppSwitchNavigator = createSwitchNavigator({
    InitialScreen: {
        screen: IntroductionScreen
    },
});

const AppNavigationContainer = createAppContainer(AppSwitchNavigator);